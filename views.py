# -----------------------------------------------------------------------------
#    Ultra Blog - Data type base blog application for Vanda platform
#    Copyright (C) 2011 Behnam AhmadKhanBeigi ( b3hnam@gnu.org )
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
# -----------------------------------------------------------------------------

from django.shortcuts import render_to_response as rr
from django.template import RequestContext
from django.conf import settings

from ultra_blog.models import Post
from ultra_blog.base import post_types
from ultra_blog.models.base import Category


def index(request):
    """
    Home page of b3hnam.com
    """
    types = post_types.get_all_admin_forms()

    result = list()
    for type_ in types:
        posts = Post.objects.filter(
            post_type_name=type_[0])[:settings.POSTS_LIMIT]
        items = list()
        items = type_[1].verbose_name, type_[1].name, posts
        result.append(items)

        cats = Category.objects.all()
    return rr('index.html',
            {"results": result,"cats":cats},
            context_instance=RequestContext(request))
