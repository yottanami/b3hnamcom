clean:
	-rm `find . -iname "*.pyc"` -v
	-rm `find . -iname "\.#*"` -v
	-rm `find . -iname "#*#"` -v

todo:
	grep "# TODO:" `find . -iname "*.py"` -Rn --color

tasks:
	grep "# IMPORTANT:" `find . -iname "*.py"` -Rn --color
	grep "# BUG:" `find . -iname "*.py"` -Rn --color
	grep "# TODO:" `find . -iname "*.py"` -Rn --color