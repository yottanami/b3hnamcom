
var hour=new Date().getHours()
var style=""
if (hour<=6) {style="night"}
if (hour>6 && hour<=12) {style="morning"}
if (hour>12 && hour<=18) {style="afternoon"}
if (hour>18 && hour<=24) {style="evening"}

if(document.createStyleSheet) {
  document.createStyleSheet('/statics/style/'+ style +'.css');
}else {
  var styles = "@import url('/statics/style/"+ style +".css');";
  var newSS=document.createElement('link');
  newSS.rel='stylesheet';
  newSS.href="/statics/style/"+ style +".css";
  document.getElementsByTagName("head")[0].appendChild(newSS);

}