import os

from django.conf.urls.defaults import patterns, include, url
from django.conf import settings

from django.contrib import admin
from ultra_blog import typediscover

admin.autodiscover()
typediscover()


urlpatterns = patterns('',
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    (r"^blog/", include("ultra_blog.urls")),
    (r'^comments/', include('django.contrib.comments.urls')),
    (r'^$', "views.index"),

)

if settings.DEBUG:
    urlpatterns += patterns('',
            (r'^statics/(?P<path>.*)$', 'django.views.static.serve',
             {'document_root': os.path.join(os.path.dirname(__file__),\
                                    'statics').replace('\\', '/')}),
)
